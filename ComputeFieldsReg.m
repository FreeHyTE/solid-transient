function [LocResults] = ComputeFieldsReg(NoDiv,Nodes,Loops,Scales,X)
% COMPUTEFIELDSREG computes and stores the spectral displacement and
% stress fields in the structure LOCRESULTS.
%
% COMPUTEFIELDSREG is called by MAINREG. It receives structures Loops and Scales, the
% solution vector X (calculated in MAINREG), the node position list Node
% and the number of divisions for plotting in the Cartesian directions,
% NoDiv (by default defined as equal to the number of Gauss-Legendre
% integration points, see INPUTPROC).
% It returns the cell of structures LOCRESULTS, containing the
% (5) spectral fields Ux, Uy, Sx, Sy, Sxy computed in a grid of (NoDiv+1)^2
% equally spaced points in every element. This means, Ux, Uy, Sx, Sy, Sxy 
% are composed of as many cells as finite elements, and in each cell we
% have a (NoDiv+1 x NoDiv+1) matrix of complex numbers.
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Bendea ED - Hybrid-Trefftz finite elements for plane structural 
% dynamics, MSc Thesis, Technical University of Cluj-Napoca, 2020.
% 3. FreeHyTE Solid Transient User's Manual - 
%    
% 4. Moldovan ID, Cismasiu I - FreeHyTE: theoretical bases and developers 
% manual, https://drive.google.com/file/d/0BxuR3pKS2hNHTzB3N2Q4cXZKcGc/vieww
%
%
% The estimates of the spectral displacement and stress fields are obtained
% by substituting the solution X of the solving system in the domain
% approximations of the respective fields. This is done element by element.
% The estimates are used for computing the time variations of the
% displacement and stress fields after multiplication by the time basis.
%
% The computation of the field estimates is further covered reference [2]. 

%% Initialization of the output points
% Generating the mesh of output points (mapped on a [-1,1] interval).  A
% total of NoDiv+1 points are chosen in each direction, to create NoDiv
% intervals. A small shift is induced to the limits of the interval in 
% order to avoid the duplication of the boundary points on adjacent 
% elements, which may compromise the visibility of the continuity gaps.
pts = linspace(-(1-1.e-5),(1-1.e-5),NoDiv+1);

% Sweeping the elements to compute the solution fields 
for ii=1:length(Loops.area)
    
    % LocLoop is a local structure where the features of the current
    % element which are directly useful for the calculation of the output
    % fields are stored.
    LocLoop = struct('id',ii,'nodes',Loops.nodes(ii,:),'center',...
        Loops.center(ii,:),'order',Loops.order(ii),'insert',...
        Loops.insert(ii,:),'dim',Loops.dim(ii,:),...
        'materials',Loops.materials(ii,:),...
        'vibration',Loops.vibration(ii,:));
    
    % Loading stiffness matrix coefficients
    k12 = LocLoop.materials(7);
    k33 = LocLoop.materials(8);
    
    % Extracting vibration features
    beta1 = LocLoop.vibration(1);
    beta2 = LocLoop.vibration(2);
    
    % Vector containing the orders of the basis
    n = -LocLoop.order:LocLoop.order;
    
    %% Generating the geometric data
    % Computing the length of the sides of the element in x and y direction.
    % Size simply collects the distances between the two most far apart 
    % points of the element in x and y directions. ASSUMES THAT THE ELEMENT 
    % IS RECTANGULAR!!
    Size = max(Nodes(LocLoop.nodes(:),:))-min(Nodes(LocLoop.nodes(:),:));
    
    % Generating the output grid in local coordinates.
    [x,y,N] = ndgrid(1/2*Size(1)*pts,1/2*Size(2)*pts,n);
    
    % Transforming the local Cartesian coordinates into polar.
    R = sqrt(x.^2 + y.^2);
    T = atan2(y,x);
    
    %% Computing the basis functions
    % Computing the displacement shape functions, in the polar referential.
    % For a full description of the basis, please refer to reference [2].
    % U1 basis
    U1r = 1/2 * beta1^-1 * (besselj(N-1,beta1*R) - besselj(N+1,beta1*R))...
        .* exp(1i*N.*T);
    U1t = 1/2 * 1i * beta1^-1 * (besselj(N-1,beta1*R) + besselj(N+1,beta1*R))...
        .* exp(1i*N.*T);
    
    % U2 basis             
    U2r = 1/2 * 1i*beta2^-1 * (besselj(N+1,beta2*R) + besselj(N-1,beta2*R))...
        .* exp(1i*N.*T);
    U2t = 1/2 * beta2^-1 * (besselj(N+1,beta2*R) - besselj(N-1,beta2*R))...
        .* exp(1i*N.*T);

    % Computing the stress shape functions, in the polar referential.
    % For a full description of the basis, please refer to reference [2].
    % S1 basis
    S1r = 1/2 * ( k33*(besselj(N-2,beta1*R)+besselj(N+2,beta1*R)-...
        2*besselj(N,beta1*R)) - 2*k12*besselj(N,beta1*R))...
        .* exp(1i*N.*T);
    S1t = 1/2 * ( - k33*(2*besselj(N,beta1*R)+besselj(N-2,beta1*R)+...
        besselj(N+2,beta1*R)) - 2*k12*besselj(N,beta1*R))...
        .* exp(1i*N.*T);
    S1rt = 1/2 * (1i*k33* ( besselj(N-2,beta1*R)-besselj(N+2,beta1*R)))...
        .* exp(1i*N.*T); 

    % S2 basis
    S2r = 1/2 * (1i* k33* (besselj(N-2,beta2*R)-besselj(N+2,beta2*R))).* exp(1i*N.*T);
    S2t = 1/2 * (-1i* k33* (besselj(N-2,beta2*R)-besselj(N+2,beta2*R))).* exp(1i*N.*T);
    S2rt = 1/2 * (-k33*( besselj(N-2,beta2*R)+besselj(N+2,beta2*R))).* exp(1i*N.*T);

    % Extracting the multipliers of each part of the basis from the X
    % solution vector
    X1(1,1,:) = X(LocLoop.insert(1):LocLoop.insert(1)+LocLoop.dim(1)-1);
    X2(1,1,:) = X(LocLoop.insert(2):LocLoop.insert(2)+LocLoop.dim(2)-1);

    %% Computing the displacement and stress fields
    % Computing the displacement and stress fields in the polar referential.
    % They are the product of the basis functions with the corresponding
    % solution vectors. 
    Ur = sum(bsxfun(@times,U1r,X1),3) + sum(bsxfun(@times,U2r,X2),3); 
    
    Ut = sum(bsxfun(@times,U1t,X1),3) + sum(bsxfun(@times,U2t,X2),3); 
    
    Sr = sum(bsxfun(@times,S1r,X1),3) + sum(bsxfun(@times,S2r,X2),3); 
    
    St = sum(bsxfun(@times,S1t,X1),3) + sum(bsxfun(@times,S2t,X2),3); 
    
    Srt = sum(bsxfun(@times,S1rt,X1),3) + sum(bsxfun(@times,S2rt,X2),3); 
       
    % Clearing the Xi variables for reuse in the next element
    clear('X1','X2','n','N');
    
    % Transforming the displacement field from the polar to the Cartesian
    % referential.
    % All pages in T are equal, so the first one is selected to compute the
    % normal cosines.
    Ux = cos(T(:,:,1)).*(Ur) - sin(T(:,:,1)).*(Ut);
    Uy = sin(T(:,:,1)).*(Ur) + cos(T(:,:,1)).*(Ut);

    % Transforming the stress field from the polar to the Cartesian
    % referential. The pore pressure is a scalar and remains unchanged
    Sx = cos(T(:,:,1)).^2.*Sr + sin(T(:,:,1)).^2.*St - ...
        2.*sin(T(:,:,1)).*cos(T(:,:,1)).*Srt;
    Sy = cos(T(:,:,1)).^2.*St + sin(T(:,:,1)).^2.*Sr + ...
        2.*sin(T(:,:,1)).*cos(T(:,:,1)).*Srt;
    Sxy = sin(T(:,:,1)).*cos(T(:,:,1)).*(Sr-St) + ...
        (cos(T(:,:,1)).^2 - sin(T(:,:,1)).^2).*Srt;
    
    % Updating the Loops structure by adding the new spectral results,
    % scaled back with the scaling coefficients
    LocResults.Ux{ii} = Ux * Scales.U;
    LocResults.Uy{ii} = Uy * Scales.U;
    LocResults.Sx{ii} = Sx * Scales.F;
    LocResults.Sy{ii} = Sy * Scales.F;
    LocResults.Sxy{ii} = Sxy * Scales.F;
   
end

end