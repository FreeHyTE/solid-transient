function Loops = ComputeMaterialProp(Loops,Scales,ohm,PlaneState)
% COMPUTEMATERIALPROP computes the material and dynamic properties and
% stores them in the Loops structure (in Loops.materials and
% Loops.vibration).
%
% COMPUTEMATERIALPROP is called by INPUTPROCREG. Receives the Loops
% structure, the spectral frequency ohm and returns the Loops structure 
% filled with the material and dynamic parameters computed in this routine.
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Bendea ED - Hybrid-Trefftz finite elements for plane structural 
% dynamics, MSc Thesis, Technical University of Cluj-Napoca, 2020.
% 3. FreeHyTE Solid Transient User's Manual - 
%    
% 4. Moldovan ID, Cismasiu I - FreeHyTE: theoretical bases and developers 
% manual, https://drive.google.com/file/d/0BxuR3pKS2hNHTzB3N2Q4cXZKcGc/view
%
% The material properties collected in Loops.materials are as follows:
%
%   No	Material property       Notation	How is it obtained?
%
%   1	Young's modulus         Young       GUI
%   2	Poisson's coefficient	Poisson     GUI
%   3	Density                 rho         GUI
%   4	Lam�'s Lambda           LL         (2.17), reference [2] (for all)
%   5	Lam�'s Mu (shear mod.)	LM         (2.18)
%   6	k11                     k11        (2.13)
%   7	k12                     k12        (2.15)(2.16)
%   8	k33                     k33        (2.14)	
%
% The dynamic properties collected in Loops.vibration are as follows:
%
%   No	Dynamic property        Wave type       Notation        Expression
%
%   1	Wave number             P               beta1           (4.11)
%   2                           S               beta2           (4.10)
%   3	Wavelength              P               wl1            
%   4                           S               wl2             
%
% The expressions refer to reference [2].

% Sweeping through the elements. The for cycle is avoidable here, but it
% may complicate matters when the rearrangement of the wave numbers takes
% place at the end. Since the lost time shouldn't be too great, the for
% cycle is left, for clarity.

%% Sweeping through the elements and computing their material and vibration 
%  characteristics
for ii=1:length(Loops.area)
    
    % To enhance the legibility of the routine, the notations in the above
    % tables are preferred over descriptions like Loops.materials(ii,jj).
    Young = Loops.materials(ii,1);
    Poisson = Loops.materials(ii,2);
    rho = Loops.materials(ii,3);
    
    % Computing the remaining material properties
    LL = Poisson*Young/(1+Poisson)/(1-2*Poisson); 
    LM = Young/2/(1+Poisson);                            
    
    % Scaling the material properties and overwriting their definitions in
    % Loops
    Young = Young/Scales.E;                  Loops.materials(ii,1) = Young;
    rho = rho/Scales.Rho;                    Loops.materials(ii,3) = rho;
    LL = LL/Scales.E;                        Loops.materials(ii,4)= LL;
    LM = LM/Scales.E;                        Loops.materials(ii,5) = LM;

    % Stiffness matrix coefficients - depend on the type of plane state
    if PlaneState == 1 
        k12 = LL*(1-2*Poisson)/(1-Poisson);  Loops.materials(ii,7)=k12;
        k33 = LM;                            Loops.materials(ii,8)=k33;
        k11 = k12 + 2*k33;                   Loops.materials(ii,6)=k11;
    else
        k12 = LL;                            Loops.materials(ii,7)=k12;
        k33 = LM;                            Loops.materials(ii,8)=k33;
        k11 = k12 + 2*k33;                   Loops.materials(ii,6)=k11;
    end
    
    % Computing the dynamic properties
    % Setting up the quadratic equation to find the proportionality
    % constants of the compressional wave
   
    % Computing the wave numbers
    beta1 = sqrt(rho/(k12 + 2*k33) * ohm^2);
    if imag (beta1) > 0
        beta1 = -1 * beta1;
    end
    beta2 = sqrt(rho/k33 * ohm^2);
    if imag (beta2) > 0
        beta2 = -1 * beta2;
    end

    % Computing the wavelengths
    wl1 = 2*pi/real(beta1);                 
    wl2 = 2*pi/real(beta2);                 
    
    Loops.vibration(ii,1)=beta1;
    Loops.vibration(ii,2)=beta2;
    Loops.vibration(ii,3)=wl1;
    Loops.vibration(ii,4)=wl2;
    
end