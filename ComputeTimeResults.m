function TimeResults = ComputeTimeResults(SpecResults,T,nel)
% COMPUTETIMERESULTS computes and stores the final displacement and
% stress fields in the dyadic points.
%
% COMPUTETIMERESULTS is called by MAINREG. It receives structure
% SpecResults, where the spectral results are stored, matrix T with the
% wavelet time basis, and the number of finite elements, NEL.
% It returns the cell of structures TIMERESULTS, containing the
% (5) fields  Ux, Uy, Sx, Sy, Sxy computed in a grid of (NoDiv+1)^2
% equally spaced points in every element, and for each dyadic point. This
% means,  Ux, Uy, Sx, Sy, Sxy are composed of as many cells as finite elements,
% and in each cell we have a (NoDiv x NoDiv x 2^Ndya+1) matrix, where Ndya
% is specified by the user in GUI1.
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Bendea ED - Hybrid-Trefftz finite elements for plane structural 
% dynamics, MSc Thesis, Technical University of Cluj-Napoca, 2020.
% 3. FreeHyTE Solid Transient User's Manual - 
%    
% 4. Moldovan ID, Cismasiu I - FreeHyTE: theoretical bases and developers 
% manual, https://drive.google.com/file/d/0BxuR3pKS2hNHTzB3N2Q4cXZKcGc/view
%
%
% The values of the fields stored in TIMERESULTS are computed according to
% expressions (3.2) and (3.4) of reference [2].

%% Sweeping through the finite elements
for ii=1:nel
    
    % Constructing the 4D matrix with the wavelet basis, to fully
    % vectorialize the code
    T4D(1,1,:,:)=T.';
    
    % Multiplying the time basis by the spectral values of the fields.
    % Only the odd-numbered spectral problems are solved, so the fields are
    % multiplied by 2 to account for the results of the even-numbered
    % problems
    TimeResults.Ux{ii} = 2*real(squeeze(sum(bsxfun(@times, ...
        SpecResults.Ux{ii}, T4D),3)));
    TimeResults.Uy{ii} = 2*real(squeeze(sum(bsxfun(@times, ...
        SpecResults.Uy{ii}, T4D),3)));
    TimeResults.Sx{ii} = 2*real(squeeze(sum(bsxfun(@times, ...
        SpecResults.Sx{ii}, T4D),3)));
    TimeResults.Sy{ii} = 2*real(squeeze(sum(bsxfun(@times, ...
        SpecResults.Sy{ii}, T4D),3)));
    TimeResults.Sxy{ii} = 2*real(squeeze(sum(bsxfun(@times, ...
        SpecResults.Sxy{ii}, T4D),3)));   
end