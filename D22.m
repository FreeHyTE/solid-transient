function LHS = D22(Edges, Loops, LHS, abscissa, weight)
% D22 sweeps through the elements and calls the functions that generate the
% D22 block of the dynamic matrix in the LHS.
%
% D22 is called by MAIN***. It receives as input data the Edges and Loops
% structures, the LHS matrix (that is, the matrix of coefficients of the
% solving system), and the Gauss-Legendre integration parameters abscissa
% and weights. It returns to MAIN*** the LHS matrix with the D22 blocks of
% all elements inserted at the correct positions (as determined in
% ASSIGNPARTS).
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Bendea ED - Hybrid-Trefftz finite elements for plane structural 
% dynamics, MSc Thesis, Technical University of Cluj-Napoca, 2020.
% 3. FreeHyTE Solid Transient User's Manual - 
%    
% 4. Moldovan ID, Cismasiu I - FreeHyTE: theoretical bases and developers 
% manual, https://drive.google.com/file/d/0BxuR3pKS2hNHTzB3N2Q4cXZKcGc/view
%
%
% D22 computes the internal product between the following bases, expressed
% in a polar (r,th) referential:
% * the displacement basis U2, 
%   U2 = | Ur | = 1/2 * | i*beta_s^-1 * (J(n+1,z_s)+J(n-1,z_s)           | * exp(i*n*th)
%        | Ut |         | beta_s^-1 * (J(n+1,z_s)-J(n-1,z_s)             |
% * the boundary traction basis N * S2, where S2 is the stress basis
%   S2 = | Sr | = 1/2 * | i*k33*(J(m-2,z_s)-J(m+2,z_s)                   | * exp(i*m*th)
%        | St |         | -i*k33*(J(m-2,z_s)-J(m+2,z_s)                  |
%        | Srt|         | -k33*(J(m-2,z_s)+J(m+2,z_s))                   |                 
%   and N is the directory cosine matrix,
%         N = | nr  0  nt |
%             | 0  nt  nr |
% In the above expressions, n and m are the line and column of the current 
% term in matrix D22; k12 and k33 are the stiffness matrix coefficients and
% depend on the type of plane state, beta_s is a vibration property of the 
% S wave (see INPUTPROC); z_s = beta_s*r; J(n,z) is the Bessel function of
% the first kind, order n and argument z; and nr and nt are the 
% radial and tangential components of the outward unit normal to the current
% boundary.
%
% As typical of the Trefftz method, the displacement and stress bases solve
% exactly the equilibrium, compatibility and elasticity equations in the
% domain of each finite element. As a direct consequence, the dynamic
% matrix can be calculated using boundary integrals only.
%
% The derivation of the bases from the solution of the (free-field) Navier
% equation is presented in reference [2].

%% Sweeping the elements

for ii=1:length(Loops.area)
    
    % LocLoop is a local structure where the features of the current
    % element which are directly useful for the calculation of the
    % dynamic block are stored.
    LocLoop = struct('id',ii,'edges',Loops.edges(ii,:), 'center',...
        Loops.center(ii,:),'order',Loops.order(ii),... 
        'insert',Loops.insert(ii,:),'dim',Loops.dim(ii,:),...
        'materials',Loops.materials(ii,:),...
        'vibration',Loops.vibration(ii,:));
    
    % Computing the D22 matrix of element ii. Function D22_MATRIX_I is a
    % local function (see below).
    D22i = D22_Matrix_i(LocLoop, Edges, abscissa, weight);
    
    % Inserting the matrix in the global LHS matrix. The insertion is made
    % at line & column Loops.insert(ii,1).
    LHS(LocLoop.insert(2):LocLoop.insert(2)+LocLoop.dim(2)-1,...
        LocLoop.insert(2):LocLoop.insert(2)+LocLoop.dim(2)-1) = D22i;
    
end

end

function D22i = D22_Matrix_i(LocLoop, Edges, abscissa, weight)
% D22_MATRIX_I local function computes the D22 dynamic block of the 
% LocLoop element. The sides are mapped to a [-1,1] interval to perform the 
% integrations.

%% Initialization 
% Initialization of the D22 block
D22i = zeros(LocLoop.dim(2));
% Loading stiffness matrix coefficients
k12 = LocLoop.materials(7);
k33 = LocLoop.materials(8);
% Loading vibration properties
beta = LocLoop.vibration(2);

% n + LocLoop.order + 1 is the current line; 
% m + LocLoop.order + 1 is the current column.
n = -LocLoop.order:LocLoop.order;
m = -LocLoop.order:LocLoop.order;

% Sweeping the edges for contour integration
for jj = 1:length(LocLoop.edges)  
    
    % identification of the jj-th edge of the loop
    id = LocLoop.edges(jj);  
    
    % LocEdge is a local structure where the features of the current
    % edge which are directly useful for the calculation of the
    % dynamic block are stored.    
    LocEdge =  struct('id',id,'nini',Edges.nini(id), 'nfin',Edges.nfin(id),...
        'parametric',Edges.parametric(id,:),'lleft',Edges.lleft(id),...
        'lright',Edges.lright(id));
    
    %% Generating the geometric data
    % The following code transforms the abscissa coordinates, expressed in
    % the [-1,1] referential, to the polar coordinates required to compute
    % the values of the basis functions. The components of the outward
    % normal to the boundary in the radial and tangential directions are
    % also calculated. They are required to transform the stress basis into
    % the traction basis.
    
    % Computing the length of the current edge
    L = sqrt(LocEdge.parametric(3)^2 + LocEdge.parametric(4)^2); 
    
    % Constructing the 3D matrices containing the n x m x abscissa
    % integration grid
    [N,M,A] = ndgrid(n,m,abscissa);
    
    % Transforming the edge abscissa into local coordinates. The local
    % referential is centered in the barycenter of the element, its axes
    % aligned with the Cartesian axes of the global referential.
    loc_x = LocEdge.parametric(1) - LocLoop.center(1) + 0.5 *...
        (A + 1) * LocEdge.parametric(3);  
    loc_y = LocEdge.parametric(2) - LocLoop.center(2) + 0.5 *...
        (A + 1) * LocEdge.parametric(4); 
    
    % Transforming the local Cartesian coordinates into polar.
    R = sqrt(loc_x.^2 + loc_y.^2);  
    T = atan2(loc_y, loc_x);
    
    % Computing the components of the outward normal in the Cartesian
    % directions.
    nx = LocEdge.parametric(4) / L;
    ny = -1* LocEdge.parametric(3) / L;
    if LocEdge.lright==LocLoop.id  % if the element is on the right,
        nx = -nx;                  % changes the sign of the normal
        ny = -ny;
    end
 
    % Computing the components of the outward normal in the polar
    % directions.
    NR = nx * cos(T) + ny * sin(T);   
    NT = -1*nx * sin(T) + ny * cos(T);
    
    
    %% Computing the basis functions for all integration points
    % Polar components of the displacement basis 
    Ur = 1i*beta^-1 * (besselj(N+1,beta*R) + besselj(N-1,beta*R))...
        .* exp(1i*N.*T);
    Ut = beta^-1 * (besselj(N+1,beta*R) - besselj(N-1,beta*R))...
        .* exp(1i*N.*T);
    % Polar components of the stress basis
    Sr =  1i* k33* (besselj(M-2,beta*R)-besselj(M+2,beta*R)).* exp(1i*M.*T);
    St = -1i* k33* (besselj(M-2,beta*R)-besselj(M+2,beta*R)).* exp(1i*M.*T);
    Srt = -k33*( besselj(M-2,beta*R)+besselj(M+2,beta*R)).* exp(1i*M.*T);
    
    %% Computing the integral on the side
    % The integral is the internal product between the displacement basis U
    % and the traction basis N * S
    
    NUS = NR.*(conj(Ur).*Sr+conj(Ut).*Srt) + ...
          NT.*(conj(Ur).*Srt+conj(Ut).*St);
       
    % Performing the side integration and updating the D22 matrix
    w3D(1,1,:) = weight;
    % computes the integral. '1/4 = 1/2*1/2' is the product of the shape
    % functions' multipliers that are not included in the expressions above
    D22i = D22i + 1/4*L/2 * sum(bsxfun(@times,NUS,w3D),3); 
 
end

end