function LHS = Da(Edges, Loops, LHS , BConds)
% Da sweeps through the edges and calls the functions that generate the Da
% blocks of the Absorbing and Robin boundaries in the LHS.
%
% Da is called by MAIN***. It receives as input data the Edges and Loops
% structures, the LHS matrix (that is, the matrix of coefficients of the
% solving system). It returns to MAIN*** the LHS matrix with the Da blocks
% of all elements inserted at the correct positions (as determined in
% ASSIGNPARTS).
%
%
% BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Bendea ED - Hybrid-Trefftz finite elements for plane structural 
% dynamics, MSc Thesis, Technical University of Cluj-Napoca, 2020.
% 3. FreeHyTE Solid Transient User's Manual - 
%    
% 4. Moldovan ID, Cismasiu I - FreeHyTE: theoretical bases and developers 
% manual, https://drive.google.com/file/d/0BxuR3pKS2hNHTzB3N2Q4cXZKcGc/view
%
%
% Da computes the internal product (Zai) between the traction basis Z of
% the Absorbing or the Robin boundary and its transpose, multiplied by a
% matrix of constants, specific to each type of boundary. Z is defined by
% Chebyshev polynomials,
%
%        Z  = cos(m*ArcCos(abscissa))
%
% and the internal product is calculated analytically.
%
% For the Absorbing boundary blocks, the matrix multiplier is designed to
% minimize the reflection of the waves from the boundary. Its coefficients
% depend of the wave propagation characteristics, as explained in Section
% 4.4.1 of reference [2]. The enforced absorbing boundary condition is
% given by expression (4.37).
%
% The expression of the Da matrix associated to an absorbing boundary is:
%
%        Da =  i * | [(k12+2k33)*beta1]^-1*Za            0          |
%                  |          0                 (k33*beta2)^-1*Za   |
%
% For Robin boundary conditions, the Da blocks are calculated multiplying the
% normal and tangential flexibilities (fn and ft) by Zai. The flexibility
% values are given in the GUI. 
% When normal or tangential flexibility coefficients are defined as NaN,
% the coresponding block does not exist.
%
% The expression of the Da matrix associated to a Robin boundary is:
%
%       Da = | -fn*Za      0       |
%            |   0      -ft*Za     |
%
% Further details on the procedure are presented in reference [2].

%% Sweeping the edges
for ii=1:length(Edges.type)
    % Da blocks are constructed for Absorbing and Robin boundaries only.
    if (strcmpi(Edges.type(ii),'A') || strcmpi(Edges.type(ii),'R'))
        % LocEdge is a local structure where the features of the current
        % edge which are directly useful for the calculation of the
        % boundary block are stored.
        LocEdge =struct('parametric',Edges.parametric(ii,:),...
            'lleft',Edges.lleft(ii),'lright',Edges.lright(ii),'order',...
            Edges.order(ii),'insert',Edges.insert(ii,:),...
            'dim',Edges.dim(ii,:),'type',Edges.type(ii));
        % Consistency check: Absorbing and Robin sides cannot have right loops
        if LocEdge.lright
            error('local:consistencyChk',...
                'Exterior edge %d cannot have a right element. \n',...
                LocEdge.id);
        end
        % Generating the Absorbing or Robin boundary block corresponding to 
        % the current edge
        if LocEdge.lleft
            id = LocEdge.lleft;
            % LocLoop is a local structure where the features of the left
            % element which are directly useful for the calculation of the
            % absorbing boundary material properties
            LocLoop = struct('id',id,'materials',Loops.materials(id,:),...
                'vibration',Loops.vibration(id,:));
        else                       % there should always be a left element
            error('local:consistencyChk',...
                'No left loop for edge %d. \n', ii);
        end
        % Launching the function that computes the non-null Absorbing or
        % Robin boundary blocks for the current edge
        [Da11,Da22] = Da_Matrix_i(LocEdge,LocLoop,BConds,ii);
        % Inserting the blocks in the global LHS matrix. The 
        % insertion starts at line LocEdge.insert(ii,1) & column 
        % LocEdge.insert(ii,1).
        % For Absorbing boundaries: Da12 and Da21 are 0. 
        % For Robin boundaries: Da12 and Da21 are 0. 
        if strcmpi(Edges.type(ii),'A')   % absorbing boundaries
             LHS(LocEdge.insert(1):LocEdge.insert(1)+LocEdge.dim(1)-1,...
                LocEdge.insert(1):LocEdge.insert(1)+LocEdge.dim(1)-1) = Da11;
             LHS(LocEdge.insert(2):LocEdge.insert(2)+LocEdge.dim(2)-1,...
                LocEdge.insert(2):LocEdge.insert(2)+LocEdge.dim(2)-1) = Da22;
        elseif (strcmpi(Edges.type(ii),'R'))  % Robin boundaries  
            % if a non-NaN flexibility was defined in the normal direction
             if LocEdge.dim(1)  % it inserts the corresponding boundary block
                LHS(LocEdge.insert(1):LocEdge.insert(1)+LocEdge.dim(1)-1,...
                    LocEdge.insert(1):LocEdge.insert(1)+LocEdge.dim(1)-1) = Da11;
             end  
             % if a non-NaN flexibility was defined in the tangential direction
             if LocEdge.dim(2)  % it inserts the corresponding boundary block
                 LHS(LocEdge.insert(2):LocEdge.insert(2)+LocEdge.dim(2)-1,...
                     LocEdge.insert(2):LocEdge.insert(2)+LocEdge.dim(2)-1) = Da22;
             end               
         end       
     end     
end
end

function [Da11, Da22] = Da_Matrix_i(LocEdge,LocLoop,BConds,ii)
% Da_MATRIX_i local function computes the Da blocks for the LocEdge edge.
% The side is mapped to a [-1,1] interval to perform the integration.
%% Generating the geometric data
% n + 1 is the current line; 
% m + 1 is the current column.
n = 0:LocEdge.order;
m = 0:LocEdge.order;

[N,M] = ndgrid(n,m);

% Computing the length of the current edge
L = sqrt(LocEdge.parametric(3)^2 + LocEdge.parametric(4)^2); % length

%% Computing the boundary integral
% Analytic expression (valid for M~=N)
Zai = (L/2 * (1./(1-(M+N).^2) + 1./(1-(M-N).^2))) .* (rem(M + N, 2)==0);
% Setting the NaN entries to zero (for M = N+1, the expression above
% yielded NaN)
Zai(isnan(Zai)) = 0;

%% Computing the Da matrix
% For Absorbing boundaries, Da blocks are calculated multiplying the
% inverse of C matrix by the boundary integral matrix Zai.
if strcmpi(LocEdge.type,'A')
        
    % Loading material properties
    k12 = LocLoop.materials(7);
    k33 = LocLoop.materials(8);
    % Loading vibration properties
    beta1 = LocLoop.vibration(1);
    beta2 = LocLoop.vibration(2);
    
    % Calculating the non-null Da components 
    
    Da11 =  1.i * ((k12+2*k33)*beta1)^-1 * Zai;
    Da22 =  1.i * (k33*beta2)^-1* Zai;

% For Robin boundaries, Da blocks are calculated multiplying the
% flexibility coefficients by the boundary integral matrix Zai.
elseif strcmpi(LocEdge.type,'R')
         
    Da11 = - BConds.Robin{ii,1} * Zai;
    Da22 = - BConds.Robin{ii,2} * Zai; 
    
end
end