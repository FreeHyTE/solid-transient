function MainReg
% MAINREG Command center of the FreeHyTE - Biphasic Transient module
% with regular rectangular meshes.
%
% MAINREG is called upon exiting the data input (GUI) phase of the module.
% It is used to call all functions required for the solution of the
% structural problem and centralize all data they provide.
%
% BIBLIOGRAPHY
% 1. FreeHyTE Page - https://sites.google.com/site/ionutdmoldovan/freehyte
% 2. Bendea ED - Hybrid-Trefftz finite elements for plane structural 
% dynamics, MSc Thesis, Technical University of Cluj-Napoca, 2020.
% 3. FreeHyTE Solid Transient User's Manual - 
%    
% 4. Moldovan ID, Cismasiu I - FreeHyTE: theoretical bases and developers 
% manual, https://drive.google.com/file/d/0BxuR3pKS2hNHTzB3N2Q4cXZKcGc/view

close all;
tic;

%% Loading the required data
load('StructDef');
load('StructRegBC1');
load('StructRegBC2');
% Generating the InputData *.mat file, storing the input data for the
% current problem.
save('InputData');

%% Processing the time-dependent input
% ohm_vec is a 2^TimeOrder vector containing the spectral frequencies.
% SpectralComponents is a matrix listing the projections of the
% time-dependent parts of the applied boundary conditions onto the wavelet
% space. SpectralComponents has as many lines as Dirichlet and Neumann
% boundaries and 2^TimeOrder+1 columns. In the first column, the index of
% the current boundary is stored.
% T is a matrix with the wavelet time basis. It has as 2^Ndya+1 lines and
% 2^TimeOrder columns. Each column corresponds to a wavelet function. Each
% line, to a dyadic time.
[ohm_vec, SpectralComponents, T] = GenTimeDepInput(TimeOrder);

% This apparently silly line is required because parfor fails to 'see'
% variables that are loaded from a mat file (transparency violation)
LNSolverQ = LNSolverQ; 

% If a parallel run is required, gets the number of workers
if parallelQ  == 1
    try
        myCluster = parcluster('local');
        parallelQ = myCluster.NumWorkers;
        clear('myCluster');
    catch
    end
end

% NoDiv is the number of points for plotting the colormaps of the
% solution, in each Cartesian direction. It is defined by the user.
NoDiv = NumberPlotPoints;

% Creating SpecResults structure to store the results for each finite
% element in each spectral run. 
for ii=1:length(loops_nodes(:,1))
    SpecResults.Ux(ii) = {zeros(NoDiv+1,NoDiv+1,2^TimeOrder)};
    SpecResults.Uy(ii) = {zeros(NoDiv+1,NoDiv+1,2^TimeOrder)};
    SpecResults.Sx(ii) = {zeros(NoDiv+1,NoDiv+1,2^TimeOrder)};
    SpecResults.Sy(ii) = {zeros(NoDiv+1,NoDiv+1,2^TimeOrder)};
    SpecResults.Sxy(ii) = {zeros(NoDiv+1,NoDiv+1,2^TimeOrder)};
end

% Also, creating SpectralDump, a sliced variable to return the data from
% the parfor loop 
SpectralDump = cell(1,length(ohm_vec)/2);


%% LAUNCHING THE SOLUTION PROCESS FOR THE SPECTRAL PROBLEMS
% The solution process is carried on in parallel or sequentially, depending
% on the value of the parallelQ variable, defined by the user in the
% STRUCTDEF GUI. If parallelQ == 0, it executes sequentially. Otherwise,
% parallelQ is the number of parallel workers summoned to execute the task.
% By default, this number is equal to the size of the machine's parallel
% pool. If you wish to use less than this number, just change parallelQ to
% the number of workers that you wish to use.
% NOTE: parallel processing in Matlab requires the Parallel Toolbox to be
% installed on the machine. If it isn't, the solution will be executed
% sequentially regardless of the value of parallelQ.

% MANUALLY SET THE NUMBER OF CORES IN CHARGE WITH THE CALCULATIONS
% parallelQ = 0;

parfor (ii = 1:length(ohm_vec)/2, parallelQ)
 % for (ii = 1:length(ohm_vec)/2)
    % only half of the runs are executed, taking advantage of the solution
    % being a real number. Because of parfor requirements, 'ii' needs to be
    % a series of integers and thus ii is not the number of the current
    % problem. This is jj=2*ii-1.
    jj = 2*ii-1;
    fprintf('Solving spectral problem %d out of %d.\n',jj,length(ohm_vec));
    
    % Extraction of the corresponding frequency and spectral components
    ohm = ohm_vec(jj);
    spectral = [SpectralComponents(:,1) SpectralComponents(:,jj+1)];
    
    %% Creating the run-dependent structures
    % Launch pre-processing routine
    % * NGP is the number of Gauss points for the line integration;
    % * Nodes is a (NNODE x 2) matrix, where NNODE is the number of nodes in
    % mesh. It stores the coordinates of each node;
    % * Edges, Loops and BConds are data structures storing information
    % on the edges, finite elements (loops) and boundary conditions,
    % respectively. They are documented in reference [4];
    [NGP, Nodes, Edges, Loops, BConds, ohm, Scales] = InputProc(ohm,spectral);
    
    % ASSIGNPARTS maps the finite element solving system and assigns entries
    % and dimensions to elements and sides. The information is used by the
    % functions that generate the blocks of the solving system to insert
    % them at the right positions.
    % * Dim is the total dimension of the finite element solving system;
    % * the mapping of the solving system is covered in reference [2].
    [Edges,Loops,Dim] = AssignParts(Edges, Loops, BConds);
    
    % Initialization of the matrix of coefficients and the free vector
    % * LHS is the matrix of coefficients of the solving system;
    % * RHS is the free vector of the solving system.
    LHS = sparse(Dim,Dim);
    RHS = zeros(Dim,1);
    
    % Initialization of Gauss-Legendre weights & abscissas (on a -1:1
    % interval). gauleg is a external routine, written by Greg von Winckel.
    [abscissa,weight] = gauleg(NGP, -1, 1);
    
    %% Generation of the solving system
    % Generation & allocation of the blocks in the matrix of coefficients.
    % General mapping of the matrix of coefficients of hybrid-Trefftz
    % elements: 
    %
    % 
    %    ________________________________            _______ 
    %   |       |       ||       |       | <------> |       |   
    %   |  D11  |  D12  ||  B1n  |  B1t  |          |   T1  |
    %   |_______|_______||_______|_______|          |_______|
    %   |       |       ||       |       |          |       |
    %   |  D21  |  D22  ||  B2n  |  B2t  |          |   T2  |
    %   |       |       ||       |       |          |       |
    %   |=======|=======|========|=======|          |=======|
    %   |       |       ||       |       |          |       |
    %   |  B1n  |  B2n  ||  Da11 |  Da12 |          |  -Un  |
    %   |_______|_______||_______|_______|          |_______|
    %   |       |       ||       |       |          |       |
    %   |  B1t  |  B2t  ||  Da21 |  Da22 |          |  -Ut  |
    %   |_______|_______||_______|_______|          |_______|
    %
    %
    %
    % The dynamic blocks have entries that correspond to the parts of the
    % bases generated by the two wave numbers corresponding to the P and
    % S waves.
    %
    % The following functions generate the coefficients blocks for each finite
    % element and essential boundary and insert them at the right place,
    % according to the mapping information generated in ASSIGNPARTS. 
    %
    % The explicit expressions of the dynamic and boundary matrices are given
    % in reference [2].
    LHS = D11(Edges, Loops, LHS, abscissa, weight);
    LHS = D12(Edges, Loops, LHS, abscissa, weight);
    LHS = D21(Edges, Loops, LHS, abscissa, weight);
    LHS = D22(Edges, Loops, LHS, abscissa, weight);
    LHS = B1(Edges, Loops, LHS, abscissa, weight);
    LHS = B2(Edges, Loops, LHS, abscissa, weight);
    LHS = Da(Edges, Loops, LHS , BConds);
    
    % Generation & allocation of the blocks in the free vector.
    % The general mapping of the free vector is consistent to that of the
    % coefficient matrix (above). The terms corresponding to the dynamic
    % matrices (Ti) represent generalized tractions applied to the Neumann
    % boundaries of the mesh. The terms corresponding to the boundary matrices
    % (U) represent generalized displacements applied to the Dirichlet
    % boundaries of the mesh, or are null if the boundary is interior,
    % Absorbing or Robin.
    %
    % The following functions generate the free vectors for each finite
    % element and essential boundary and insert them at the right place,
    % according to the mapping information generated in ASSIGNPARTS.
    %
    % The explicit expressions of the free vectors are given in reference [2].
    RHS = T1(Edges, Loops, BConds, RHS, abscissa, weight);
    RHS = T2(Edges, Loops, BConds, RHS, abscissa, weight);
    RHS = U(Edges, BConds, RHS, abscissa, weight); 
    
    %% Pre-conditioning and storage of the solving system
    %
    % Pre-conditioning and storage of the solving system
    %
    % * the solving system is pre-conditioned using a scaling procedure
    % aimed at reducing its diagonal elements to unity while preserving the
    % symmetry of the original system;
    % * the scaling procedure is described in Section 5.4 of reference [2];
    % * the system is stored in a sparse form.
    %
    % Generating the scaling matrix, Sc. Sc is a diagonal matrix, whose
    % terms are defined as the inverse of the square roots of the diagonal
    % terms of the coefficient matrix.
    Sc = sqrt(diag(LHS)).^-1; %Sc = ones(size(LHS,1),1);
    
    % If the diagonal term is null, the respective line and column remain
    % unscaled.
    Sc(isinf(Sc)) = 1;
    
    % Going sparse
    Sc = sparse(diag(Sc));
    
    % Scaling the LHS matrix. Old LHS is overwritten by its scaled version
    % in order to save memory 
    LHS = Sc' * LHS * Sc; 
    % Scaling the RHS vector. Old RHS is overwritten by its scaled version
    % in order to save memory     
    RHS = Sc' * RHS;
    
   
    
    %% Solution of the solving system
    
    % To detect ill-conditioned systems, an estimate of the condition
    % number of the matrix of coefficients is calculated. If the reciprocal
    % of the condition number is smaller than the precision of the machine,
    % the system is considered ill-conditioned.
    %
    % The default Matlab solver (mldivide) is used for the solution of the
    % solving system, whether it is well or ill-conditioned, unless the
    % user specifically opts for a minimum norm solution procedure in the
    % STRUCTDEF GUI. MLDIVIDE is much faster than (a well converged)
    % minimum norm procedure and tends to work quite well. The minimum norm
    % solver should only be used when MLDIVIDE fails, and even so, it is
    % probably a better idea to simply increase the h-refinement and
    % decrease the p-refinement to stabilize the system.
    %
    % The minimum norm procedure LSQR returns the solution that iteratively
    % minimizes the norm of the error (ScLHS * ScX - ScRHS), 
    % * below a threshold defined below, or 
    % * until the error fails to subside in a series of iterations, or
    % * until the maximum number of iterations specified below is
    % reached.
    
    % Computing the estimate of the reciprocal of the condition number
    CndNo=condest(LHS)^-1;
    
    % LSQR is only used if the system is ill-conditioned and the user
    % specifically requested that it is used
    if (CndNo<eps && LNSolverQ == 1)
        warning('local:NumericalChk',...
            'System condition number is %0.5g. Solving with least norm solver.\n',...
            CndNo);
        % MINIMUM NORM SOLUTION PROCEDURE. 1e-6 is the recommended
        % threshold. The maximum number of iterations is defined as 2000.
        % This is a very time-consuming procedure if it goes all the way to
        % 2000.
        X = lsqr(LHS,RHS,1e-6,2000);
    else
        % Default solution procedure
          
        X = LHS\RHS;

    end

    % Reverting the scaling of the solution
    X = Sc * X;
    
    
    %% Post-processing
    
    % Construct the displacement and stress fields.
    % COMPUTEFIELDSREG returns the values of the fields in a NoDiv x NoDiv
    % grid in each element.
    % SpectralDump is a cell of structures. It has as many structures
    % inside as spectral problems actually solved, that is, 2^(TimeOrder-1)
    % cause only half of the spectral problems are actually solved. This
    % architecture is required to ensure the variable is sliced, so parfor
    % can operate on it.
    % For each spectral problem, the corresponding structure contains the
    % (8) fields Ux, Uy, Sx, Sy, Sxy computed in a grid of (NoDiv+1)^2
    % equally spaced points in every element. This means, Ux, Uy, Sx, Sy, Sxy
    % are composed of as many cells as finite elements, and in each cell we
    % have a (NoDiv x NoDiv) matrix of complex numbers.
    SpectralDump{ii} = ComputeFieldsReg(NoDiv,Nodes,Loops,Scales,X);
   
end

% The main reason SpectralDump looks like it does is that parfor wants it
% sliced. However, there is no longer any need to keep it that way.
% Therefore, the new structure of arrays SPECRESULTS is created to store
% the spectral results. SpecResults.Ux(etc) contains as many 3D matrices as
% finite elements. Each 3D matrix has (NoDiv x NoDiv) elements on every
% page (containing the field values in the output points), and 2^TimeOrder
% pages, one for each spectral problem. The copying of SpectralDump into
% SpecResults is made using the following formula:
% SpecResults.Ux{ee}(:,:,ss) = SpectralDump{1,ss}.Ux{ee}
% where ee = 1:length(loops_nodes(:,1)) is the index of the element, and
%       ss = 1:2:length(ohm_vec) is the index of the spectral run
%       (ss+1)/2 is the old 'ii' from the parfor loos, that is, the current
%       page of SpectralDump
for ee = 1:length(loops_nodes(:,1))
    for ss = 1:2:length(ohm_vec)
        SpecResults.Ux{ee}(:,:,ss) = SpectralDump{1,(ss+1)/2}.Ux{ee};
        SpecResults.Uy{ee}(:,:,ss) = SpectralDump{1,(ss+1)/2}.Uy{ee};
        SpecResults.Sx{ee}(:,:,ss) = SpectralDump{1,(ss+1)/2}.Sx{ee};
        SpecResults.Sy{ee}(:,:,ss) = SpectralDump{1,(ss+1)/2}.Sy{ee};
        SpecResults.Sxy{ee}(:,:,ss) = SpectralDump{1,(ss+1)/2}.Sxy{ee};
    end
end
clear('SpectralDump');

% Computing the TimeResults structure where the fields' values for all
% finite elements and dyadic times are stored. TimeResults is a structure
% with the same members as SpecResults. Each member is a cell containing as
% many 3D matrices as finite elements. The matrix associated to a finite
% element is NDIV*NDIV*(2^NDYA+1), storing the values of the respective
% field in each solution point, and for each dyadic instant.
TimeResults = ComputeTimeResults(SpecResults,T,size(loops_nodes,1));
clear('SpecResults');

% Getting the mesh information for plotting
[~, Nodes, Edges, Loops, ~, ~, Scales] = InputProc(1,[SpectralComponents(:,1)... 
    SpectralComponents(:,2)]);

% Sweeping the dyadic points for plotting. Plotting is only triggered if
% the user requested written output or a plot is requested at the current
% dyadic point.
for ii = 1:size(T,1)
    if ~isempty(DirName) || (PlotStep && ~rem(ii,PlotStep))
        PlotFieldsReg(TimeResults,NoDiv,Nodes,Edges,Loops,Scales,ii,Nx,Ny);
    end
end

PlotTime = toc;
fprintf('\t Successful exit in %g seconds.\n',PlotTime);

% House cleaning: restoring warnings, cleaning the input file 
warning('on','MATLAB:DELETE:FileNotFound');
warning('on','MATLAB:load:variableNotFound');
warning('on','MATLAB:rmpath:DirNotFound');
warning('on','MATLAB:dispatcher:pathWarning');
delete InputData.mat;

end
