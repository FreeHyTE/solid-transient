function [PSI,T,ZH] = Generate_Basis_File(nfam,j0,pval)

% Alternative - read base_phi & mat_derivada from file
fileID = fopen('w_base','r');
sizeA = [2^pval+1 2^(j0+1)];
sizeA = fliplr(sizeA);
base_phi = fscanf(fileID,'%g',sizeA); 
base_phi = base_phi';
base_phi(:,2^j0+1:end) = [];

fileID = fopen('w_deriv','r');
sizeA = [2^(j0+1) 2^(j0+1)];
mat_derivada = fscanf(fileID,'%g',sizeA); 
mat_derivada = mat_derivada';
mat_derivada = mat_derivada(1:2^j0,1:2^j0);

% Matrix h
h = eye(2^j0);

% Matrix g
g = base_phi(end,:)'*base_phi(end,:)-mat_derivada;

% Eigenanalysis of h
[E,Lambda] = eig(h);
E = fliplr(E);

% Matrix g*
gstar = E*g*E;

% Eigenvalues of G*
[Estar,PSI]=eig(gstar);
PSI=diag(PSI);

% Z matrix for the T basis
Z = Estar.'*E.';

% Basis T
T = base_phi*Z.';

% Inverse of the H matrix
Hm1 = inv(Estar'*Estar);

% ZH matrix
ZH = Hm1*conj(Z);

% computing psi
psi = ZH*base_phi(1,:).';

end
