 function [ap_fe]=aprox_fe(nfam,j0,pval)
% 
%  [ap_fe]=aprox_fe(nfam,j0,p)
%
%  Rotina que determina a aproximação de uma função considerando uma base constituída pelas 
%  funções de escala a um determinado grau de refinamento, j0
%
%% Definição da base

[base_phi]=base_fe(nfam,j0,pval);


%% Inicializações

n_intervalos=2^pval;
npontos=2^pval+1;
delta=1/n_intervalos;

nfunc=2^j0;

x(1)=0.0;
for i=2:npontos
   x(i)=x(i-1)+delta;
end

%% Definição da aproximação

for i=1:nfunc
   soma=0.0;
   for j=1:npontos
      soma=soma+base_phi(j,i)*1/(0.1+x(j))^2;
%      soma=soma+base_phi(j,i)*x(j)^3;
   end
   coef(i)=soma*delta;
end

%% Figuras

figure(1)
plot(coef)

ap_fe=base_phi*coef';

figure(2)
plot(x,ap_fe)

return