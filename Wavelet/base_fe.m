 function [base_phi]=base_fe(nfam,j0,pval)
% 
%  [base_phi]=base_fe(nfam,j0,p)
%
%  Função que determina a base constituída pelas funções de escala a um 
%  determinado grau de refinamento, j0.

%% Inicializações

npontos=2^pval+1;
nfunc=2^j0;
base_phi(1:npontos,1:nfunc)=0.0;

p_entrada=pval-j0;
np_loc=2^p_entrada*(2*nfam-1)+1;

%% Definição das funções na extremidade esquerda do intervalo

[phi_left_x,~]=phi_left_j(nfam,j0,p_entrada);
base_phi(1:np_loc,1:nfam)=phi_left_x;

%% Definição das funções no domínio

for i=1:nfunc-2*nfam
   iloc_i=i*2^p_entrada+1;
   iloc_j=iloc_i+np_loc-1;
   [phi_x,~,~,~]=phi_jk(nfam,j0,i,p_entrada);
   base_phi(iloc_i:iloc_j,nfam+i)=phi_x';
end

%% Definição das funções na extremidade direita do intervalo

[phi_right_x,~]=phi_right_j(nfam,j0,p_entrada);
for i=1:nfam
   for j=1:np_loc
      base_phi(npontos-j+1,nfunc-i+1)=phi_right_x(j,i);
   end
end


%% Construção dos gráficos

x(1:npontos)=0.0;

x(1)=0.0;
delta=(2*nfam-1)/2^pval;
for i=2:npontos
   x(i)=x(i-1)+delta;
end

%plot(base_phi);
