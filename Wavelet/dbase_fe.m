 function [dbase_phi]=dbase_fe(nfam,j0,pval)
% 
%  [dbase_phi]=dbase_fe(nfam,j0,p)
%
%  Função que determina a derivada das funcoes da base constituída pelas funções de escala a um 
%  determinado grau de refinamento, j0.
%
%% Inicializações

npontos=2^pval+1;

nfunc=2^j0;

dbase_phi(1:npontos,1:nfunc)=0.0;

p_entrada=pval-j0;
np_loc=2^p_entrada*(2*nfam-1)+1;

%% Valores das funções na extremidade esquerda do intervalo

[dphi_left_x,~]=dphi_left_j(nfam,j0,p_entrada);
dbase_phi(1:np_loc,1:nfam)=dphi_left_x;

%% Valores das funções no domínio

for i=1:nfunc(1)-2*nfam
   iloc_i=i*2^p_entrada+1;
   iloc_j=iloc_i+np_loc-1;
   [~,dphi_x,~,~]=phi_jk(nfam,j0,i,p_entrada);
   dbase_phi(iloc_i:iloc_j,nfam+i)=dphi_x';
end


%% Valores das funções na extremidade direita do intervalo

[dphi_right_x,~]=dphi_right_j(nfam,j0,p_entrada);
for i=1:nfam
   for j=1:np_loc
      dbase_phi(npontos-j+1,nfunc-i+1)=dphi_right_x(j,i);
   end
end

%% Traçado de gráficos

x(1:npontos)=0.0;

x(1)=0.0;
delta=(2*nfam-1)/2^pval;
for i=2:npontos
   x(i)=x(i-1)+delta;
end

plot(dbase_phi);
