 function [ro]=int_fe(nfam)
% 
%  [integrais_fe]=int_fe(nfam)
%
%  Função que determina o valor dos integrais envolvendo produtos de funções de escala
%  e suas derivadas.


%% Inicializações

TOL=10^-6;
[h]=daubechies;


%%
% Determinacao da matriz M
%
ilim=2*nfam-1;
inic_l=2-2*nfam;
ifim_l=2*nfam-2;
numero_l=ifim_l-inic_l+1;

for i=1:numero_l
    for j=1:numero_l
        sis(i,j)=0.0;
    end
end

for l=1:numero_l
    val_l=l-ilim;
    for i=0:ilim
        for j=0:ilim 
            ipos=2*val_l+i-j;
            ipos=ipos+ilim;
            if ipos>0 && ipos < numero_l+1
                sis(l,ipos)=sis(l,ipos)+h(i+1,nfam)*h(j+1,nfam)*2;
            end
        end
    end
end

[V,D]=eig(sis);

for ii=1:numero_l
    valor=D(ii,ii);
    valor2=abs(1.0-valor);
    if valor2 < TOL
        soma=0.0;
        for jj=1:numero_l
            rr(jj)=V(jj,ii);
            soma=soma+rr(jj)*(jj-ilim);
        end
    end
end


ro=-rr/soma;

ro(2*nfam-1)=0;