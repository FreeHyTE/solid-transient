 function [mat_beta_0,mat_alfa_0,mat_beta_1,mat_alfa_1]=int_fe_int(nfam)
% 
%  [mat_beta_0,mat_alfa_0,mat_beta_1,mat_alfa_1]=int_fe_int(nfam)
%
%  Função que determina o valor dos integrais envolvendo produtos de funções de escala
%  e suas derivadas no extremo esquerdo do intervalo


%%
% Calculo dos integrais envolvendo funcoes de escala interiores
%
ilim=2*nfam-1;
inic_l=2-2*nfam;
ifim_l=2*nfam-2;
numero_l=ifim_l-inic_l+1;

[ro]=int_fe(nfam);

[H]=daubechies;
h=H(1:2*nfam,nfam);

%% Cálculo dos Mr pág 5
for r=1:nfam-1
   sum=0;
   sum2=0;
   for m=-nfam+1:nfam
      sum=sum+h(m+nfam)*(m)^(r)/factorial(r);
      sum2=sum2+h(1-m+nfam)*(m)^(r)/factorial(r);
   end
   M(r)=sum/sqrt(2);
   M2(r)=sum2/sqrt(2);
end

%%
%Cálculo de Cl pág 5 e Mr
%

C(1)=1;
for l=1:nfam-1
   sum=0;
   L=l+1;
   for r=1:l
      sum=sum+M(r)*C(l-r+1);
   end
   C(L)=sum/((2^l)-1);
end
C;

C2(1)=1;
for l=1:nfam-1
   sum=0;
   L=l+1;
   for r=1:l
      sum=sum+M2(r)*C2(l-r+1);
   end
   C2(L)=sum/((2^l)-1);
end
C;


%%
%Cálculo dos Pl pág 5
%

for X=0:3*nfam-2
   Xp=X+1;
   for l=0:nfam-1
      sum=0;
      sum2=0;
      L=l+1;
      for n=0:l
         Np=n+1;
         sum=sum+C(l-n+1)*X^(n)/factorial(n);
         sum2=sum2+C2(l-n+1)*X^(n)/factorial(n);
     end      
     P(L,Xp)=sum;  
     PP(L,Xp)=sum2;
 end 
end

for X=1-nfam:nfam-1
   Xp=X+nfam;
   for l=0:nfam-1
      sum=0;
      L=l+1;
      for n=0:l
         Np=n+1;
         sum=sum+C(l-n+1)*X^(n)/factorial(n);    
      end      
      P2(L,Xp)=sum;      
   end 
end

for X=1-nfam:nfam-1
   Xp=X+nfam;
   for l=0:nfam-1
      sum=0;
      L=l+1;
      for n=0:l
         Np=n+1;
         sum=sum+C2(l-n+1)*(X)^(n)/factorial(n);    
      end      
      P3(L,Xp)=sum;      
   end 
end

%%
% Calculo de Bphi
%
for i=1:nfam
    for j=1:2*nfam-2
        Bphi(i,j)=0.0;
    end
end

for ii=0:nfam-1
    for jj=nfam:3*nfam-3
        iloc=ii+1;
        jloc=jj-nfam+1;
        soma=0.0;
        for k=1-nfam:nfam-1
            vteste=k-jj;
            if vteste <= ifim_l & vteste >= inic_l
                soma=soma+ro(vteste+2*nfam-1)*P2(ii+1,k+nfam);
            end
        end
        Bphi(iloc,jloc)=soma;
    end
end

for ii=0:nfam-1
    for jj=nfam:3*nfam-3
        iloc=ii+1;
        jloc=jj-nfam+1;
        soma=0.0;
        for k=1-nfam:nfam-1
            vteste=k-jj;
            if vteste <= ifim_l & vteste >= inic_l
                soma=soma-ro(vteste+2*nfam-1)*P3(ii+1,k+nfam);
            end
        end
        Bphir(iloc,jloc)=soma;
    end
end



%%Matriz b pág 8
%
for i=0:nfam-1
   linha=i+1;
   for j=nfam:3*nfam-2
      coluna=j-nfam+1;
      sum=P(i+1,j+1)/(2^i);
      sum2=PP(i+1,j+1)/(2^i);
      for m=nfam:fix((j+nfam-1)*.5)
         sum=sum-2^.5*P(i+1,m+1)*h(j-2*m+nfam);
         sum2=sum2-2^.5*PP(i+1,m+1)*h(1-(j-2*m)+nfam);
      end
      b(linha,coluna)=sum;
      b2(linha,coluna)=sum2;
   end
end

%% calculo da matriz grameana - fim da página 13

M1=ones(nfam,nfam);
M=zeros(nfam,nfam);
Diag=zeros(nfam,nfam);

for i=1:nfam
   for j=1:nfam
      M(i,j)=2^(-(i+j-2));
   end
   Diag(i,i)=1./2^(i-1);
end

Y=(b*b');
Y2=(b2*b2');
Z=(2*M1-M);
Z2=(M1-M);

for i=1:nfam
   for j=1:nfam
      G(i,j)=Y(i,j)/Z(i,j);
      G2(i,j)=Y2(i,j)/Z(i,j);
   end
end


%%
% Calculo de mat_beta
%

GSI=G^(-(1/2));
GSI2=G2^(-(1/2));
mat_beta_0=GSI*Bphi;
matbeta=GSI2*Bphir;
for ii=1:nfam
    for jj=1:2*nfam-2
        mat_beta_1(ii,jj)=matbeta(nfam-ii+1,2*nfam-1-jj);
    end
end
%
%%
% Calculo da matriz Aphi
%
for ii=1:nfam
    for jj=1:nfam
        Aphi(ii,jj)=0.0;
    end
end

for ii=1:nfam
    for jj=1:2*nfam-1
        Bnovo(ii,jj)=0.0;
        Bnovor(ii,jj)=0.0;
    end
end

for ii=1:nfam
    for jj=1:2*nfam-2
        Bnovo(ii,jj)=Bphi(ii,jj);
        Bnovor(ii,jj)=Bphir(ii,jj);
    end
end

for ii=1:2*nfam-1
    for jj=1:2*nfam-1
        difere=ii-jj;
        if difere>= 2-2*nfam & difere <= 2*nfam-2 
            romat(ii,jj)=ro(difere+2*nfam-1);
        end
    end
end

Y=-b*Bnovo'*Diag+Diag*Bnovo*b'+b*romat*b';
Y2=-b2*Bnovor'*Diag+Diag*Bnovor*b2'-b2*romat*b2';

for ii=1:nfam
   for jj=1:nfam
       if ii == 1 & jj == 1
           Aphi(ii,jj)=-0.5;
           Aphir(ii,jj)=0.5;
       else
           Aphi(ii,jj)=Y(ii,jj)/Z2(ii,jj);
           Aphir(ii,jj)=Y2(ii,jj)/Z2(ii,jj);
       end
   end
end

Aphi;

%%
% Determinacao da matriz mat_alfa

mat_alfa_0=GSI*Aphi*GSI;
matalfa=GSI2*Aphir*GSI2;
for ii=1:nfam
    for jj=1:nfam
        mat_alfa_1(ii,jj)=matalfa(nfam-ii+1,nfam-jj+1);
    end
end

