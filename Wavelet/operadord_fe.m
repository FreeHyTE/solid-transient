 function [mat_derivada]=operadord_fe(nfam,j0)

% 
%  [mat_derivada]=operadord_fe(nfam,j0)
%
%  Rotina que efectua a construšao da matriz com o operador diferencial f*f'

nloc=2^j0-2*nfam;

ndim=2^j0;

isalto=nloc-(2*nfam-2);

for ii=1:ndim
    for jj=1:ndim
        mat_derivada(ii,jj)=0.0;
    end
end

[ro]=int_fe(nfam);

[mat_beta_0,mat_alfa_0,mat_beta_1,mat_alfa_1]=int_fe_int(nfam);

%
%  Espalhamento das matrizes A0 e A1
%
for ii=1:nfam
    for jj=1:nfam
        mat_derivada(ii,jj)=mat_alfa_0(ii,jj);
        mat_derivada(ii+2^j0-nfam,jj+2^j0-nfam)=mat_alfa_1(ii,jj);
    end
end

%
% Espalhamento da matriz B0
%
for ii=1:nfam
    for jj=1:2*nfam-2
        mat_derivada(ii,jj+nfam)=mat_beta_0(ii,jj);
    end
end

%
% Espalhamento da matriz -B0^t
%
for ii=1:2*nfam-2
    for jj=1:nfam
        mat_derivada(ii+nfam,jj)=-mat_beta_0(jj,ii);
    end
end
%
% Espalhamento do bloco B1
%
for ii=1:nfam
    for jj=1:2*nfam-2
        mat_derivada(ii+2^j0-nfam,nfam+isalto+jj)=mat_beta_1(ii,jj);
    end
end
%
% Espalhamento do bloco -B1^t
%
for ii=1:2*nfam-2
    for jj=1:nfam
        mat_derivada(nfam+isalto+ii,jj+2^j0-nfam)=-mat_beta_1(jj,ii);
    end
end

%
% Espalhamento do bloco r
%

for ii=1:nloc
    for jj=1:nloc
        difere=ii-jj;
        if difere >= 2-2*nfam & difere <= 2*nfam-2
            mat_derivada(ii+nfam,jj+nfam)=ro(difere+2*nfam-1);
        end
    end
end

mat_derivada=2^j0*mat_derivada;




