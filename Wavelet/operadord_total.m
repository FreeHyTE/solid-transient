 function [mat_derivada]=operadord_total(nfam,j0,jmax)

% 
%  [mat_derivada]=operadord_total(nfam,j0,jmax)
%
%  Rotina que efectua a construšao da matriz com o operador diferencial f*f'


jtotal=jmax+1;
ntransform=jmax-j0;

ndim1=2^jtotal;
ndim2=2^jmax;

for ii=1:ndim1
    for jj=1:ndim1
        mat_derivada(ii,jj)=0.0;
    end
end

[mat_derivada]=operadord_fe(nfam,jtotal);

[mat_P1,mat_P2]=projectores(nfam,jtotal);


Bloco11=mat_P1*mat_derivada*mat_P1';
Bloco12=mat_P1*mat_derivada*mat_P2';
Bloco21=mat_P2*mat_derivada*mat_P1';
Bloco22=mat_P2*mat_derivada*mat_P2';

for ii=1:ndim2
    for jj=1:ndim2
        mat_derivada(ii,jj)=Bloco11(ii,jj);
        mat_derivada(ii+ndim2,jj+ndim2)=Bloco22(ii,jj);
        mat_derivada(ii,jj+ndim2)=Bloco12(ii,jj);
        mat_derivada(ii+ndim2,jj)=Bloco21(ii,jj);
    end
end

jacumula=0;

for itr=1:ntransform
    
    jota=jmax-itr+1;
    jacumula=jacumula+2^jota;
    
    ndim1=2^jota;
    ndim2=2^(jota-1);
    ndim3=jacumula;
    
    clear mat_P1;
    clear mat_P2;
    clear Bloco11;
    clear Bloco22;
    clear Bloco12;
    clear Bloco21;
    
    for ii=1:ndim1
        for jj=1:ndim1
            M1(ii,jj)=mat_derivada(ii,jj);
        end
    end
    
    for ii=1:ndim1
        for jj=1:ndim3
            M2(ii,jj)=mat_derivada(ii,jj+ndim1);
        end
    end
    
    for ii=1:ndim3
        for jj=1:ndim1
            M3(ii,jj)=mat_derivada(ii+ndim1,jj);
        end
    end
    
    [mat_P1,mat_P2]=projectores(nfam,jota);
    
    Bloco11=mat_P1*M1*mat_P1';
    Bloco12=mat_P1*M1*mat_P2';
    Bloco21=mat_P2*M1*mat_P1';
    Bloco22=mat_P2*M1*mat_P2';
    
    Bs1=mat_P1*M2;
    Bs2=mat_P2*M2;
    
    Bi1=M3*mat_P1';
    Bi2=M3*mat_P2';
    
    for ii=1:ndim2
        for jj=1:ndim2
            mat_derivada(ii,jj)=Bloco11(ii,jj);
            mat_derivada(ii+ndim2,jj+ndim2)=Bloco22(ii,jj);
            mat_derivada(ii,jj+ndim2)=Bloco12(ii,jj);
            mat_derivada(ii+ndim2,jj)=Bloco21(ii,jj);
        end
    end


    for ii=1:ndim2
        for jj=1:ndim3
            mat_derivada(ii,jj+ndim1)=Bs1(ii,jj);
            mat_derivada(ii+ndim2,jj+ndim1)=Bs2(ii,jj);
        end
    end
    
    for ii=1:ndim3
        for jj=1:ndim2
            mat_derivada(ii+ndim1,jj)=Bi1(ii,jj);
            mat_derivada(ii+ndim1,jj+ndim2)=Bi2(ii,jj);
        end
    end
    
    clear M1;
    clear M2;
    clear M3;
    
    clear Bs1;
    clear Bs2;
    clear Bi1;
    clear Bi2;
    
end

spy(mat_derivada);

