 function [mat_P1,mat_P2]=projectores(nfam,j0)

% 
%  [mat_P1,mat_P2]=projectores(nfam,j0)
%
%  Rotina que determina as matrizes de projeccao P1 e P2


nloc=2^j0-2*nfam;
nloc2=2^(j0-1)-2*nfam;

ndim=2^j0;
ndim2=2^(j0-1);

for ii=1:ndim2
    for jj=1:ndim
        mat_P1(ii,jj)=0.0;
        mat_P2(ii,jj)=0.0;
    end
end

[H0,h0,G0,g0]=val_coef_left(nfam);
[H1,h1,G1,g1]=val_coef_right(nfam);
[H]=daubechies;

h=H(1:2*nfam,nfam);


for ii=1:2*nfam
    g(ii)=h(2*nfam-ii+1)*(-1)^(ii+1);
end

%
% Construšao do projector P1
%

%
%  Espalhamento das matrizes H0 e H1
%
for ii=1:nfam
    for jj=1:nfam
        mat_P1(ii,jj)=H0(ii,jj);
        mat_P1(ii+2^(j0-1)-nfam,jj+2^j0-nfam)=H1(nfam-ii+1,nfam-jj+1);
    end
end

%
% Espalhamento da matriz h0
%
for ii=1:nfam
    for jj=1:2*nfam-1
        mat_P1(ii,jj+nfam)=h0(ii,jj);
    end
end

%
% Espalhamento do bloco h1
%
isalto=2^j0-2*nfam-(2*nfam-1);
for ii=1:nfam
    for jj=1:2*nfam-1
        mat_P1(ii+2^(j0-1)-nfam,nfam+isalto+jj)=h1(nfam-ii+1,(2*nfam-1)-jj+1);
    end
end

%
% Espalhamento do bloco H
%

for ii=1:nloc2
    for jj=1:nloc
        indice=-nfam+1+jj-2*ii;
        if indice >= 1-nfam & indice <= nfam
            mat_P1(ii+nfam,jj+nfam)=h(indice+nfam);
        end
    end
end



%
% Construšao do projector P2
%

%
%  Espalhamento das matrizes G0 e G1
%
for ii=1:nfam
    for jj=1:nfam
        mat_P2(ii,jj)=G0(ii,jj);
        mat_P2(ii+2^(j0-1)-nfam,jj+2^j0-nfam)=G1(nfam-ii+1,nfam-jj+1);
    end
end

%
% Espalhamento da matriz g0
%
for ii=1:nfam
    for jj=1:2*nfam-1
        mat_P2(ii,jj+nfam)=g0(ii,jj);
    end
end

%
% Espalhamento do bloco g1
%
isalto=2^j0-2*nfam-(2*nfam-1);
for ii=1:nfam
    for jj=1:2*nfam-1
        mat_P2(ii+2^(j0-1)-nfam,nfam+isalto+jj)=g1(nfam-ii+1,(2*nfam-1)-jj+1);
    end
end

%
% Espalhamento do bloco G
%

for ii=1:nloc2
    for jj=1:nloc
        indice=-nfam+1+jj-2*ii;
        if indice >= 1-nfam & indice <= nfam
            mat_P2(ii+nfam,jj+nfam)=g(indice+nfam);
        end
    end
end

