 function [val_int]=quadratura(nfam,j0,timefunc,dt)
% 
%  [val_int]=quadratura(nfam,j0)
%
%  Fun��o que determina o valor dos integrais aplicando as regras de quadratura

% Inicializa�oes

q=nfam;
n_pesos=q+1;
denominador=2^j0;

% C�lculo dos pesos da regra de quadratura
[w,wleft,wright]=wquad(nfam);

val=q/2;
for ii=1:n_pesos
    x(ii)=-val+ii-1;
    x2(ii)=ii-1;
end

%
% Determina�ao dos integrais com as fun�oes no limite esquerdo
%

for ifunc=1:nfam
    soma=0.0;
    for p=0:q
        vt=x2(p+1)/denominador;
        soma=soma+wleft(p+1,ifunc)*timefunc(vt,dt);
    end
    val_int(ifunc)=soma/2^(j0/2);
end
%
% Determinacao das integracoes com as funcoes interiores
%

ipos1=nfam;
ipos2=2^j0-1-nfam;
for ifunc=ipos1:ipos2
    soma=0.0;
    for p=0:q
        vt=(ifunc+x(p+1))/denominador;
        soma=soma+w(p+1)*timefunc(vt,dt);
    end
    val_int(ifunc+1)=soma/2^(j0/2);
end

%
% Determina�ao dos integrais com as fun�oes no limite direito
%

for ifunc=1:nfam
    soma=0.0;
    for p=0:q
        vt=1-(x2(q-p+1))/denominador;
        soma=soma+wright(p+1,ifunc)*timefunc(vt,dt);
    end
    val_int(2^j0-nfam+ifunc)=soma/2^(j0/2);
end

val_int;
