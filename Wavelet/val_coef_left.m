%Calcula os ceoficientes ho e Ho pag 14 de Valerie
%
%[H0,h0,G0,g0]=val_coef_left(N)
% 
%N=no. de familia de Daubechies
%
function [H0,h0,G0,g0]=val_coef_left(N)

%Calculo da matriz D

D=zeros(N,2*N-1);

for i=1:N
   D(i,i)=1/(2^(i-1));
end   

[H]=daubechies;
h=H(1:2*N,N);

%c�lculo dos Mr p�g 5
for r=1:N-1
   sum=0;
   for m=-N+1:N
      sum=sum+h(m+N)*(m)^(r)/factorial(r);
   end
   M(r)=sum/sqrt(2);
end
M;
%
%C�lculo de Cl p�g 5 e Mr
%

C(1)=1;
for l=1:N-1
   sum=0;
   L=l+1;
   for r=1:l
      sum=sum+M(r)*C(l-r+1);
   end
   C(L)=sum/((2^l)-1);
end
C;

%
%C�lculo dos Pl p�g 5
%

for X=0:3*N-2
   Xp=X+1;
   for l=0:N-1
      sum=0;
      L=l+1;
      for n=0:l
         Np=n+1;
         sum=sum+C(l-n+1)*X^(n)/factorial(n);    
      end      
      P(L,Xp)=sum;      
   end 
end
P;

%Matriz b p�g 8
for i=0:N-1
   linha=i+1;
   for j=N:3*N-2
      coluna=j-N+1;
      sum=P(i+1,j+1)/(2^i);
      for m=N:fix((j+N-1)*.5)
         sum=sum-2^.5*P(i+1,m+1)*h(j-2*m+N);
      end
      b(linha,coluna)=sum;
   end
end
b;

% calculo da matriz grameana - fim da p�gina 13

M1=ones(N,N);
M=zeros(N,N);
for i=1:N
   for j=1:N
      M(i,j)=2^(-(i+j-2));
   end
end

Y=(b*b');
Z=(2*M1-M);

for i=1:N
   for j=1:N
      G(i,j)=Y(i,j)/Z(i,j);
   end
end

%c�lculo de H0 e h0 da p�g 14, eq. 23

GSI=G^(-(1/2));
GS=G^(1/2);

% Gteste=inv(sqrtm(G))-GSI

H0=zeros(N,N);
h0=zeros(N,2*N-1);

DN=zeros(N,N);

for i=1:N
   DN(i,i)=1/(2^(i-1));
end   

H0=2^(-.5)*GSI*DN*GS;
h0=2^(-.5)*GSI*b;

%c�lculo de G0 e g0 da p�g 14, eq. 25
g1 = eye(N) - H0' * H0;
g2 = - H0' * h0;
gram = g1 * g1' + g2 * g2';
% Orthonormalisation de Gram
gram2 = inv(sqrtm(gram));
G0 = gram2 * g1;
g0 = gram2 * g2;

