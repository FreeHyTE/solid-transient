 function [w,wleft,wright]=wquad(nfam)
% 
%  [w,wleft,wright]=wquad(nfam)
%
%  Fun��o que determina o valor dos pesos das regras de quadratura envolvendo
%  fun�oes interiores e fun�oes de extremidade.

% Inicializa�oes

q=nfam;
n_pesos=q+1;

for i=1:n_pesos
    for j=1:n_pesos
        mat(i,j)=0.0;
    end
end

val=q/2;
for ii=1:n_pesos
    x(ii)=-val+ii-1;
    x2(ii)=ii-1;
    x3(ii)=1-x2(ii);
end

%
% Determina�ao da matriz do sistema
%


for i=1:n_pesos
    for j=1:n_pesos
        mat(i,j)=x(j)^(i-1)/factorial(i-1);
        matb(i,j)=x2(j)^(i-1)/factorial(i-1);
        matc(i,j)=x3(j)^(i-1)/factorial(i-1);
    end
end


%
%  Determina�ao dos termos independentes
%

[H]=daubechies;
h=H(1:2*nfam,nfam);

%c�lculo dos Mr p�g 5
for r=1:nfam
   sum=0;
   sum2=0;
   for m=-nfam+1:nfam
      sum=sum+h(m+nfam)*(m)^(r)/factorial(r);
      sum2=sum2+h(1-m+nfam)*(m)^(r)/factorial(r);
   end
   M(r)=sum/sqrt(2);
   M2(r)=sum2/sqrt(2);
end
M;
%
%C�lculo de Cl p�g 5 e Mr
%

C(1)=1;
for l=1:nfam
   sum=0;
   L=l+1;
   for r=1:l
      sum=sum+M(r)*C(l-r+1);
   end
   C(L)=sum/((2^l)-1);
end

C2(1)=1;
for l=1:nfam
   sum=0;
   L=l+1;
   for r=1:l
      sum=sum+M2(r)*C2(l-r+1);
   end
   C2(L)=sum/((2^l)-1);
end

for i=1:n_pesos
    ti(i)=C(i);
end

w=mat\ti';

[H0,h0,G0,g0]=val_coef_left(nfam);
[H1,h1,G1,g1]=val_coef_right(nfam);


%
%  Determinacao de wleft
%

%  Inicializa�oes
%
for ii=1:nfam
    for jj=1:n_pesos
        wleft(ii,jj)=0;
        wright(ii,jj)=0;
        matriz_X(ii,jj)=0;
        matriz_X2(ii,jj)=0;
    end
end

clear ti;
clear mat2;
clear mat3;

for p=0:q
    for ii=nfam:3*nfam-2
        for jj=0:p
            mat2(ii-nfam+1,jj+1)=ii^(p-jj)/factorial(p-jj);
        end
    end
    for ii=0:p
        ti(ii+1)=C(ii+1);
        ti2(ii+1)=C2(ii+1);
    end
    vector=h0*mat2*ti';
    vector2=h1*mat2*ti2';
    sis=eye(nfam);
    sis2=eye(nfam);
    for ii=1:nfam
        for jj=1:nfam
            sis(ii,jj)=2^(p+0.5)*sis(ii,jj)-H0(ii,jj);
            sis2(ii,jj)=2^(p+0.5)*sis2(ii,jj)-H1(ii,jj);
        end
    end
    vectorX=sis\vector;
    vectorX2=sis2\vector2;

    icoluna=p+1;
    for ii=1:nfam
        matriz_X(ii,icoluna)=vectorX(ii);
        matriz_X2(ii,icoluna)=vectorX2(ii);
    end
    clear ti;
    clear mat2;
    clear mat3;
    clear vector;
    clear sis;
    clear vectorX;
end


wleft=(matriz_X*inv(matb'))';

waux=(matriz_X2*inv(matb'))';

%wright=(matriz_X2*inv(matc'))';

clear wright

for ii=1:n_pesos
    for jj=1:nfam
       wright(ii,jj)=waux(n_pesos-ii+1,nfam-jj+1);
   end
end
       